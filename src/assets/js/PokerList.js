/**
 * 模拟手牌类
 */
class PokerList {
	constructor(arg) {
		this.pokerList1=PokerList.pokerList1();
		this.pokerList2=PokerList.pokerList2()
	}
	
	static pokerList1(){
		return [{"number": 3,"text": "3","type": 3},{"number": 3,"text": "3","type": 3},{"number": 3,"text": "3","type": 3}, {"number": 3,"text": "3","type": 2}, {"number": 5,"text": "5","type": 3}, {"number": 5,"text": "5","type": 0}, {"number": 5,"text": "5","type": 0},  {"number": 7,"text": "7","type": 2}, {"number": 8,"text": "8","type": 3}, {"number": 9,"text": "9","type": 0}, {"number": 10,"text": "10","type": 2}, {"number": 10,"text": "10","type": 1}, {"number": 11,"text": "J","type": 2}, {"number": 11,"text": "J","type": 1}, {"number": 12,"text": "Q","type": 2}, {"number": 12,"text": "Q","type": 3}, {"number": 13,"text": "K","type": 1}, {"number": 13,"text": "K","type": 3}, {"number": 14,"text": "A","type": 2}, {"number": 15,"text": "2","type": 3}]
	}
	
	static pokerList2(){
		let that = this
		let arr=['3','4','5','6','7','8','10','5','6','7','8','9','10','A','2','S','X']
		let arrR=[]
		for (let k in arr) {
			let item
			let number=that.textToNumber(arr[k])
			let text=arr[k]
			item=`{"number": ${number},"text":"${text}","type": 1}`
			arrR.push(JSON.parse(item))
		}
		arrR.sort(PokerList.sortFunction);
		return arrR
	}
	
	/**
	 * 牌排序
	 */
	static sortFunction(a, b) {
		return a.number - b.number;
	}
	
	static textToNumber(text) {
		switch (text) {
			case '3':
				return 3;
			case '4':
				return 4;
			case '5':
				return 5;
			case '6':
				return 6;
			case '7':
				return 7;
			case '8':
				return 8;
			case '9':
				return 9;
			case '10':
			case '0':
			case 'T':
			case 't':
				return 10;
			case '11':
			case 'j':
			case 'J':
				return 11;
			case '12':
			case 'q':
			case 'Q':
				return 12;
			case '13':
			case 'k':
			case 'K':
				return 13;
			case '1':
			case 'a':
			case 'A':
				return 14;
			case '2':
				return 15;
			case 's':
			case 'S':
				return 16;
			case 'x':
			case 'X':
				return 17;
		}
	}
	
	static numberToText(number) {
		switch (number) {
			case 3:
				return '3';
			case 4:
				return '4';
			case 5:
				return '5';
			case 6:
				return '6';
			case 7:
				return '7';
			case 8:
				return '8';
			case 9:
				return '9';
			case 10:
				return '10';
			case 11:
				return 'J';
			case 12:
				return 'Q';
			case 13:
				return 'K';
			case 14:
				return 'A';
			case 15:
				return '2';
			case 16:
				return 'S'; //小王
			case 17:
				return 'X'; //大王
		}
	}
	
}

export default PokerList;