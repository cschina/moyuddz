import Vue from 'vue'
import App from './App.vue'
import "@/assets/css/excel.css";
const $ = require('jquery');
import JsonViewer from 'vue-json-viewer'

Vue.config.productionTip = false;
Vue.use(JsonViewer)

new Vue({
  render: h => h(App),
}).$mount('#app');
