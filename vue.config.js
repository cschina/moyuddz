const webpack = require('webpack');
module.exports = {
	lintOnSave: false,
	publicPath: './',
	configureWebpack: {
		plugins: [
			// 配置jquery
			new webpack.ProvidePlugin({
				$: "jquery",
				jQuery: "jquery",
				"windows.jQuery": "jquery"
			}),
		]
	}
};
